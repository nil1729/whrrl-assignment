// AUTH REDUCERS
export const LOAD_USER = 'LOAD_USER';
export const LOG_OUT = 'LOG_OUT';
export const AUTH_ALERTS = 'AUTH_ALERTS';
export const CLEAR_ALERTS = 'CLEAR_ALERTS';
export const UPDATE_PROFILE = 'UPDATE_PROFILE';
