import axios from 'axios';
let instance = axios.create({
	baseURL: 'http://localhost:5050/api/v1',
	timeout: 5000,
});

if (process.env.NODE_ENV === 'production') {
	instance = axios.create({
		baseURL: 'https://ancient-anchorage-58491.herokuapp.com/api/v1',
		// timeout: 5000,
	});
}

const createConfig = () => {
	return {
		headers: {
			'Content-Type': 'application/json',
			'x-auth-token': localStorage.getItem('AUTH_TOKEN'),
		},
	};
};

export default instance;
export { createConfig };
