import { LOAD_USER, LOG_OUT } from '../types';
import firebase from '../../firebase/firebaseApp';
import sendRequest, { createConfig } from '../utils/axios-setup';

const loadUser = () => async (dispatch) => {
	try {
		firebase.auth().onAuthStateChanged(async function (user) {
			if (user) {
				const idToken = await firebase.auth().currentUser.getIdToken(true);
				localStorage.setItem('AUTH_TOKEN', idToken);
				try {
					const result = await sendRequest.post('/auth/login', {}, createConfig());
					return dispatch({
						type: LOAD_USER,
						payload: {
							user: result.data,
						},
					});
				} catch (e) {
					console.log(e);
				}
			} else {
				return dispatch({
					type: LOG_OUT,
				});
			}
		});
	} catch (e) {
		console.log(e);
	}
};

const signOut = () => async (dispatch) => {
	try {
		await firebase.auth().signOut();
	} catch (e) {
		console.log(e);
	}
};

const updateProfile = (data) => {
	try {
		return {
			type: LOAD_USER,
			payload: {
				user: data,
			},
		};
	} catch (e) {
		console.log(e);
	}
};

export { loadUser, signOut, updateProfile };
