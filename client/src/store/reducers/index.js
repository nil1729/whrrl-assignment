import { combineReducers } from 'redux';
import auths from './auths';

export default combineReducers({
	AUTH_STATE: auths,
});
