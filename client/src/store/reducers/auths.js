import { LOAD_USER, LOG_OUT } from '../types';
const initialState = {
	isAuthenticated: false,
	loading: true,
	user: null,
	alerts: {},
};

export default (state = initialState, action) => {
	switch (action.type) {
		case LOAD_USER:
			return {
				...state,
				isAuthenticated: true,
				loading: false,
				user: action.payload.user,
			};
		case LOG_OUT:
			return {
				...state,
				isAuthenticated: false,
				loading: false,
				user: null,
			};
		default: {
			return state;
		}
	}
};
