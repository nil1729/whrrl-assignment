import React, { useState, useEffect } from 'react';
import styles from './dashboard.module.scss';
import { connect } from 'react-redux';
import { Input, Select, message } from 'antd';
import validator from 'validator';
import downloadIcon from '../../assets/icon-1.png';
import sendRequest, { createConfig } from '../../store/utils/axios-setup';
import { updateProfile, signOut } from '../../store/actions/auths';
const { Option } = Select;

const Dashboard = ({ authState: { user }, updateProfile, signOut }) => {
	function handleChange(value) {
		setUsetInputs({ ...userInputs, nationality: value });
	}

	const [userInputs, setUsetInputs] = useState({
		name: '',
		email: '',
		phone: '',
		nationality: 'indian',
	});

	useEffect(() => {
		setUsetInputs({
			name: user?.name || '',
			email: user?.email_address || '',
			phone: user?.phone_number || '',
		});
	}, [user]);

	const [fileName, setFileName] = useState('Choose File');
	const [file, setFile] = useState('');
	const [isLoading, setIsLoading] = useState(false);

	const handleSubmit = async (e) => {
		e.preventDefault();
		if (!file) {
			message.error('Please select a file for aadhar card');
			return;
		} else if (
			!userInputs.phone ||
			(userInputs.phone && !validator.isMobilePhone(userInputs.phone, 'en-IN'))
		) {
			message.error('Please enter valid phone number');
			return;
		}

		const formData = new FormData();
		formData.append('phone_number', userInputs.phone);
		formData.append('nationality', userInputs.nationality);
		formData.append('aadhar', file);

		try {
			setIsLoading(true);
			const result = await sendRequest.post('/dashboard/upload', formData, createConfig());
			updateProfile(result.data.user_details);
			setIsLoading(false);
		} catch (e) {
			console.log(e);
		}
	};

	const handleChangeInput = (e) => {
		setUsetInputs({ ...userInputs, [e.target.name]: e.target.value });
	};
	const fileChange = (e) => {
		setFile(e.target.files[0]);
		setFileName(e.target.files[0].name);
	};

	return (
		<>
			<div className={styles.main_content}>
				<div className={styles.side_bar}>
					<div className={styles.menus}>
						<div className={styles.menu_item}>
							<div className={styles.menu_item_icon}>
								<i className='fas fa-home'></i>
							</div>
							<div className={styles.menu_item_text}>
								<p>Dashboard</p>
							</div>
							<div className={styles.menu_item_icon_right}>
								<i className='fas fa-arrow-right'></i>
							</div>
						</div>
					</div>
					<div className={styles.help_menus}></div>
					<div className={styles.settings}>
						<p style={{ color: '#f2f2f2', margin: '0 0px 0 10px' }}>Settings</p>
						<div className={styles.settings_item} onClick={signOut}>
							<div className={styles.menu_item_icon}>
								<i className='fas fa-user'></i>
							</div>
							<div className={styles.menu_item_text}>
								<p>Profile</p>
							</div>
						</div>
						<div className={styles.settings_item} onClick={signOut}>
							<div className={styles.menu_item_icon}>
								<i className='fas fa-cog'></i>
							</div>
							<div className={styles.menu_item_text}>
								<p>Log Out</p>
							</div>
						</div>
					</div>
				</div>
				<div className={styles.main_layout}>
					<h2>Welcome,</h2>
					<h2>Nilanjan Deb</h2>
					<div className={styles.image_container}>
						<img src={user?.profile_picture?.public_url} alt='' />
					</div>
					{user?.profile_completed ? (
						<h2>Thank you for the submission. Please check the information below</h2>
					) : (
						<h2>Kindly help us with the following information</h2>
					)}
					<form>
						<div className={styles.form_container}>
							<div className={styles.form_row}>
								<div className={styles.form_item}>
									<Input
										placeholder='Full Name'
										onChange={handleChangeInput}
										name='name'
										value={userInputs.name}
										readOnly
									/>
								</div>
								<div className={styles.form_item}>
									<Input
										placeholder='Email Id'
										onChange={handleChangeInput}
										name='email'
										value={userInputs.email}
										readOnly
									/>
								</div>
							</div>
							<div className={styles.form_row}>
								<div className={styles.form_item}>
									<Input
										placeholder='Phone Number'
										onChange={handleChangeInput}
										name='phone'
										value={userInputs.phone}
										type='number'
										readOnly={user?.profile_completed}
									/>
								</div>
								<div className={styles.form_item}>
									<Select
										defaultValue='indian'
										style={{ width: '100%' }}
										onChange={handleChange}
										disabled={user?.profile_completed}
									>
										<Option value='indian'>Indian</Option>
										<Option value='american'>American</Option>
										<Option value='canadian'> Canadian</Option>
									</Select>
								</div>
							</div>
							<div className={styles.form_row}>
								{user?.profile_completed ? (
									<h4>
										<a href={user?.aadhar_document?.public_url} download>
											<img src={downloadIcon} alt='' />
											Download Aadhar
										</a>
									</h4>
								) : (
									<input
										type='file'
										name=''
										required
										onChange={fileChange}
										accept='.png,.jpg,.pdf'
									/>
								)}
							</div>
							{user?.profile_completed ? null : (
								<div className={styles.form_row}>
									<button type='submit' onClick={handleSubmit} disabled={isLoading}>
										{isLoading ? 'Loading ...' : 'Submit'}
									</button>
								</div>
							)}
						</div>
					</form>
				</div>
			</div>
		</>
	);
};

const mapStateToProps = (state) => ({
	authState: state.AUTH_STATE,
});

export default connect(mapStateToProps, { updateProfile, signOut })(Dashboard);
