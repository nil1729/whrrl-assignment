import React, { useEffect } from 'react';
import firebase, { facebookProvider, googleProvider } from '../../firebase/firebaseApp';
import styles from './login.module.scss';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';

const Login = ({ authState: { isAuthenticated, details } }) => {
	const history = useHistory();

	useEffect(() => {
		if (isAuthenticated) {
			history.push('/');
		}
		// eslint-disable-next-line
	}, [isAuthenticated]);

	const handleClick = (provider) => async () => {
		try {
			await firebase.auth().signInWithPopup(provider);
		} catch (e) {
			console.log(e);
		}
	};

	return (
		<div className={styles.main_body}>
			<h3 className={styles.header_title}>Welcome,</h3>
			<div className={styles.auth_btn_container}>
				<button
					className={`${styles.auth_btn} ${styles.fb_btn}`}
					onClick={handleClick(facebookProvider)}
				>
					Sign in with Facebook
				</button>
				<button
					className={`${styles.auth_btn} ${styles.google_btn}`}
					onClick={handleClick(googleProvider)}
				>
					Sign in with Google
				</button>
			</div>
		</div>
	);
};

const mapStateToProps = (state) => ({
	authState: state.AUTH_STATE,
});

export default connect(mapStateToProps)(Login);
