import React, { useEffect } from 'react';
import Login from './pages/Login/Login';
import Dashboard from './pages/Dashboard/Dashboard';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import PrivateRoute from './Routes/PrivateRoute';
import { connect } from 'react-redux';
import { loadUser } from './store/actions/auths';
import './App.css';

const App = ({ loadUser }) => {
	useEffect(() => {
		loadUser();
		// eslint-disable-next-line
	}, []);
	return (
		<Router>
			<Switch>
				<Route path='/login' exact component={Login} />
				<PrivateRoute path='/' exact component={Dashboard} />
			</Switch>
		</Router>
	);
};

export default connect(null, { loadUser })(App);
