const express = require('express');
const chalk = require('chalk');
const morgan = require('morgan');
const connectDB = require('./config/db');
const errorHandler = require('./middleware/errorHandler');
const mongoSanitize = require('express-mongo-sanitize');
const xss = require('xss-clean');
const rateLimit = require('express-rate-limit');
const hpp = require('hpp');
const cors = require('cors');
const cookieParser = require('cookie-parser');

// Load Env Vars
if (process.env.NODE_ENV !== 'production') {
	const dotenv = require('dotenv');
	dotenv.config({ path: './config/config.env' });
}

// Initialize App
const app = express();

// Logging Middleware
app.use(morgan('dev'));

// Cookie Parser Middleware
app.use(cookieParser());

// Body Parser Setup
app.use(express.json());

// To remove data, use:
app.use(mongoSanitize());

// Remove XSS
app.use(xss());

// Limit the Request per minutes
app.use(
	rateLimit({
		windowMs: 15 * 60 * 1000,
		max: 100,
	})
);

// Prevent Http Params Pollution
app.use(hpp());

// Enable Cors
app.use(cors());

// Use Routes in app
app.use('/api/v1', require('./routes'));

// Error Handler Middleware
app.use(errorHandler);

// Serve "Public" folder as a static directory
app.use(express.static(__dirname + '/public'));

// PORT Setup and Server Setup on PORT
const PORT = process.env.PORT || 5050;
const server = app.listen(PORT, async () => {
	console.log(
		chalk.yellowBright.bold(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`)
	);
	const db = await connectDB();
	app.set('db_object', db);
});

// Handle unhandled Promise rejections
process.on('unhandledRejection', (err) => {
	console.log(chalk.bold.redBright(`Error: ${err.message}`));
	process.env.NODE_ENV === 'production'
		? server.close(() => {
				console.log(chalk.bold.redBright('Server closed due to unhandled promise rejection'));
				process.exit(1);
		  })
		: console.log(err);
});
