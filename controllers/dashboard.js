const ErrorResponse = require('../utils/ErrorResponse');
const asyncHandler = require('../middleware/asyncHandler');
const bcrypt = require('bcrypt');
const validator = require('validator');
const fs = require('fs');
const User = require('../models/User');
const { uploadFileToFirebase } = require('./handleFacebookImage');

exports.uploadDetails = asyncHandler(async (req, res, next) => {
	const aadharFile = req.file;
	const uploadOption = {
		destination: `users/${req.user_details.email}/aadhar-${req.file.filename}`,
		uploadType: 'media',
		metadata: {
			metadata: {
				contentType: req.file.mimetype,
				uploadedBy: req.user_details.name,
			},
		},
	};
	const public_url = await uploadFileToFirebase(aadharFile.path, uploadOption);
	const aadhar_document = {
		file_id: uploadOption.destination,
		public_url,
	};
	await User.updateOne(
		{ email_address: req.user_details.email },
		{
			$set: {
				aadhar_document,
				phone_number: req.body.phone_number,
				nationality: req.nationality,
				profile_completed: true,
			},
		}
	);
	let user_doc = await User.findOne({ email_address: req.user_details.email });
	return res.status(200).json({
		success: true,
		message: 'Profile updated successfully',
		user_details: user_doc,
	});
});
