const Fs = require('fs');
const Path = require('path');
const Axios = require('axios');
const firebaseAdmin = require('../config/firebaseAdmin');
const storageBucket = firebaseAdmin.storage().bucket();

exports.downloadImage = async (fb_url, rnd_str) => {
	const path = Path.resolve(__dirname, '..', 'tmp', `${rnd_str}.jpeg`);
	const writer = Fs.createWriteStream(path);

	const response = await Axios({
		url: fb_url,
		method: 'GET',
		responseType: 'stream',
	});

	response.data.pipe(writer);

	return new Promise((resolve, reject) => {
		writer.on('finish', resolve);
		writer.on('error', reject);
	});
};

exports.uploadFileToFirebase = async (filePath, uploadOptions) => {
	try {
		await storageBucket.upload(filePath, uploadOptions);
		const publicURL = await storageBucket.file(uploadOptions.destination).getSignedUrl({
			action: 'read',
			expires: '03-09-2491',
		});
		Fs.unlinkSync(filePath);
		return publicURL[0];
	} catch (error) {
		console.log(error);
		// throw new ErrorResponse(error.message, 500);
	}
};
