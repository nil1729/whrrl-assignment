const express = require('express');
const router = express.Router();
const validator = require('validator');
const asyncHandler = require('../middleware/asyncHandler');
const verifyAuth = require('../middleware/authentication');
const User = require('../models/User');
const { downloadImage, uploadFileToFirebase } = require('./handleFacebookImage');
const randomString = require('randomstring');
const path = require('path');

exports.loginHandler = asyncHandler(async (req, res, next) => {
	let existing_user = await User.findOne({ email_address: req.user_details.email });
	if (existing_user) {
	} else {
		let profile_picture = { public_url: req.user_details.picture };
		if (req.user_details.firebase.sign_in_provider === 'facebook.com') {
			const rnd_str = randomString.generate(10);
			await downloadImage(req.user_details.picture, rnd_str);
			const filePath = path.resolve(__dirname, '..', 'tmp', `${rnd_str}.jpeg`);
			const uploadOption = {
				destination: `users/${req.user_details.email}/profile-picture-${rnd_str}.jpeg`,
				uploadType: 'media',
				metadata: {
					metadata: {
						contentType: 'image/jpeg',
						uploadedBy: req.user_details.name,
					},
				},
			};
			let public_url = await uploadFileToFirebase(filePath, uploadOption);
			profile_picture = {
				file_id: uploadOption.destination,
				public_url: public_url,
			};
		}

		existing_user = await User.create({
			email_address: req.user_details.email,
			name: req.user_details.name,
			profile_picture,
			provider: [req.user_details.firebase.sign_in_provider],
		});
	}
	return res.json(existing_user);
});
