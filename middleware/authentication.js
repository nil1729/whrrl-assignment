const admin = require('../config/firebaseAdmin');

module.exports = async (req, res, next) => {
	try {
		if (!req.headers['x-auth-token']) {
			return res.status(403).json({
				msg: 'Unauthorized Access',
			});
		}
		const token = req.headers['x-auth-token'];
		const decodedToken = await admin.auth().verifyIdToken(token);
		req.user_details = decodedToken;
		next();
	} catch (e) {
		return res.status(403).json({
			msg: 'Invalid Credentials',
		});
	}
};
