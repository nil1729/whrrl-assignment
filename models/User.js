const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const UserSchema = mongoose.Schema(
	{
		name: {
			type: mongoose.SchemaTypes.String,
			required: [true, 'Please add a name'],
			match: [/^[A-Za-z\s]+$/, 'Only alphabetic characters are allowed'],
			trim: true,
			maxlength: [50, 'Please choose a name which has at most 50 characters'],
		},
		email_address: {
			type: mongoose.SchemaTypes.String,
			match: [
				/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
				'Please provide a valid email address',
			],
			index: {
				unique: true,
				partialFilterExpression: { email_address: { $type: 'string' } },
			},
		},
		phone_number: {
			type: mongoose.SchemaTypes.String, //+91-XXXXX-XXXXX
		},
		nationality: {
			type: mongoose.SchemaTypes.String,
		},
		profile_picture: {
			file_id: { type: mongoose.SchemaTypes.String },
			public_url: { type: mongoose.SchemaTypes.String },
		},
		aadhar_document: {
			file_id: { type: mongoose.SchemaTypes.String },
			public_url: { type: mongoose.SchemaTypes.String },
		},
		provider: {
			type: mongoose.SchemaTypes.Array,
			required: true,
		},
		profile_completed: {
			type: mongoose.SchemaTypes.Boolean,
			default: false,
		},
	},
	{ timestamps: true }
);

// Match the Password
UserSchema.methods.matchPassword = async function (password) {
	return await bcrypt.compare(password, this.password);
};

module.exports = mongoose.model('User', UserSchema);
