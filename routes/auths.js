const express = require('express');
const router = express.Router();

const { loginHandler } = require('../controllers/auth');
const verifyToken = require('../middleware/authentication');

router.route('/login').post(verifyToken, loginHandler);

module.exports = router;
