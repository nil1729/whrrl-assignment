const express = require('express');
const router = express.Router();

const { uploadDetails } = require('../controllers/dashboard');
const verifyToken = require('../middleware/authentication');
const multer = require('../config/multerSetup');

router.route('/upload').post(verifyToken, multer.single('aadhar'), uploadDetails);

module.exports = router;
